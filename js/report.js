





var lunaMini = new Flickity(".diagrams-carousel", { 
    contain: !0, 
    pageDots: !1, 
    // wrapAround: !0, 
    imagesLoaded: !0,
    prevNextButtons: false
});

$(document).ready(function(){
    $("a[href*=#]").on("click", function(e){
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1000);
        e.preventDefault();
        return false;
    });
});

$('.percent:eq(0)').spincrement({
    duration: 2000
});

let anim1 = true;
let anim2 = true;
$(window).scroll(function(){
    var wt = $(window).scrollTop();
	var wh = $(window).height();
	var et = $('#diagram-uncrit-debt').offset().top;
    var eh = $('#diagram-uncrit-debt').outerHeight();
    var et1 = $('.diagrams-carousel').offset().top;
	var eh2 = $('.diagrams-carousel').outerHeight();
	var dh = $(document).height();   
	if (anim1 && (wt + wh >= et + 100 || wh + wt == dh || eh + et < wh)){
        anim1 = false;
        $('.uncritical-debt').addClass('analis1');
        $('.critical-debt').addClass('analis2');
        $('#uncrit-debt').spincrement({
            duration: 2000,
            thousandSeparator: ' ',
            decimalPoint: '.'
        });
        $('#all-debt').spincrement({
            duration: 2000,
            thousandSeparator: ' ',
            decimalPoint: '.'
        });
        $('#crit-debt').spincrement({
            duration: 2000,
            thousandSeparator: ' ',
            decimalPoint: '.'
        });
    }
    if (anim2 && (wt + wh >= et + 100 || wh + wt == dh || eh + et < wh)){
        anim2 = false;
        $('.carousel-cell:eq(0)').addClass('detail1');
        $('.carousel-cell:eq(1)').addClass('detail2');
        $('.carousel-cell:eq(2)').addClass('detail3');
        $('.carousel-cell:eq(3)').addClass('detail4');
        $('.carousel-cell:eq(4)').addClass('detail5');
    }
});
